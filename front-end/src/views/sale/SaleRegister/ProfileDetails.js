import React, { useState,useEffect } from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import axios from 'axios';
import { Link } from 'react-router-dom'
import Select from 'react-select';
import InputLabel from '@material-ui/core/InputLabel';
import {
  Box,
  Button,
  Card,
  CardContent,
  CardHeader,
  Divider,
  Grid,
  TextField,
  makeStyles,
  MenuItem
} from '@material-ui/core';

const states = [
  {
    value: 'alabama',
    label: 'Alabama'
  },
  {
    value: 'new-york',
    label: 'New York'
  },
  {
    value: 'san-francisco',
    label: 'San Francisco'
  }
];

const useStyles = makeStyles(() => ({
  root: {}
}));

const ProfileDetails = ({ className, ...rest }) => {
  const classes = useStyles();
  const [values, setValues] = useState([]);
    //const [customers] = useState(data);
    const [customs,setCustoms] = useState([]);
    const [products, setProducts] = useState([]);
    const [didMount, setDidMount] = useState(false); 
  
    //useEffect(()=>{
      //axios.get('http://localhost:8080/api/userlabs')
      //.then(res=>{
        //console.log(res)
        //setCustomers(res.data)
     //})
    //})
  
  
    useEffect(() => {
  
      const fetchData = async () => {
         const result = await axios(
          'http://localhost:8080/api/userlabs',
          
        );

      result.data = result.data.map(o=>{
      o.label = o.nombre;
          o.value = o.id;
          return o;
        })
        setCustoms(result.data);
      };

      fetchData();
      
      const fetchData2 = async () => {
        const result2 = await axios(
         'http://localhost:8080/api/products',
         
       );

     result2.data = result2.data.map(o=>{
     o.label = o.descripcion;
         o.value = o.id;
         return o;
       })
       setProducts(result2.data);
     };

     fetchData2();
     
    }, []);




  const handleChangeSelect = (value) =>
  {
    let event = {};
    event.target = {};
    event.target.name = "id_cliente";
    event.target.value = value.value;
    handleChange(event);
  }

  const handleChangeSelect2 = (value) =>
  {
    let event = {};
    event.target = {};
    event.target.name = "id_producto";
    event.target.value = value.value;
    handleChange(event);
  }


  const handleChange = (event) => {
    setValues({
      ...values,
      [event.target.name]: event.target.value
    });
  };
  
  const handleSubmit = (event) => {
    alert('A name was submitted: ');
    console.log(values);
    event.preventDefault();
    componentDidMount();
  }

  const componentDidMount = () => {
    // POST request using axios with error handling
    const article = { title: 'React POST Request Example' };
    axios.post('http://localhost:8080/api/sales', values)
    .catch(error => {
        //this.setState({ errorMessage: error.message });
        console.error('There was an error!', error);
    });
}



  return (
    <form
      autoComplete="off"
      noValidate
      className={clsx(classes.root, className)}
      onSubmit={handleSubmit}
      {...rest}
    >
      <Card>
        <CardHeader
          subheader="Ingrese la Información del la Venta"
          title="Registro"
        />
        <Divider />
        <CardContent>
          <Grid
            container
            spacing={3}
          >
            <Grid
              item
              md={6}
              xs={12}
            >
              
        <InputLabel id="demo-simple-select-label">Cliente</InputLabel>
              <Select
                fullWidth
                label="Nombre"
                name="nombre"
                options={customs}
                onChange={handleChangeSelect}
                required
                
                variant="outlined"
              >
             
                </Select>
            </Grid>
            <Grid
              item
              md={6}
              xs={12}
            >
               <InputLabel id="demo-simple-label">Producto</InputLabel>
              <Select
                fullWidth
                label="Nombre"
                name="nombre"
                options={products}
                onChange={handleChangeSelect2}
                required
                
                variant="outlined"
              >
                </Select>
            </Grid>
            <Grid
              item
              md={6}
              xs={12}
            >
              <TextField
                fullWidth
                label="cantidad"
                name="cantidad"
                onChange={handleChange}
                required
                variant="outlined"
              />
            </Grid>
            <Grid
              item
              md={6}
              xs={12}
            >
              <TextField
                fullWidth
                label="Precio"
                name="precio"
                onChange={handleChange}
                type="number"
                required
                variant="outlined"
              />
            </Grid>
           

            <Grid
              item
              md={6}
              xs={12}
            >

            </Grid>
          </Grid>
        </CardContent>
        <Divider />
        <Box
          display="flex"
          justifyContent="flex-end"
          p={2}
        >
          <Button
            color="primary"
            variant="contained"
            onClick ={handleSubmit}
          >
          <Link style={{ color: '#FFF' }}  to='/app/sale' >  Guardar </Link>
          </Button>
        </Box>
      </Card>
    </form>
  );
};

ProfileDetails.propTypes = {
  className: PropTypes.string
};

export default ProfileDetails;

