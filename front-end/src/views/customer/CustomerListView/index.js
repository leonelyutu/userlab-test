import React, { useState,useEffect } from 'react';
import axios from 'axios';
import {
  Box,
  Container,
  makeStyles
} from '@material-ui/core';
import Page from 'src/components/Page';
import Results from './Results';
import Toolbar from './Toolbar';
import data from './data';

const useStyles = makeStyles((theme) => ({
  root: {
    backgroundColor: theme.palette.background.dark,
    minHeight: '100%',
    paddingBottom: theme.spacing(3),
    paddingTop: theme.spacing(3)
  }
}));

const CustomerListView = () => {

  const classes = useStyles();
  //const [customers] = useState(data);
  const [customers,setCustomers] = useState([])
  const [didMount, setDidMount] = useState(false); 

  //useEffect(()=>{
    //axios.get('http://localhost:8080/api/userlabs')
    //.then(res=>{
      //console.log(res)
      //setCustomers(res.data)
   //})
  //})


  useEffect(() => {
    const fetchData = async () => {
      const result = await axios(
        'http://localhost:8080/api/userlabs',
      );
 
      setCustomers(result.data);
    };
 
    fetchData();
  }, []);

  useEffect(() => {
    setDidMount(true);
    return () => setDidMount(false);
 }, [])
 
 if(!didMount) {
   return null;
 }
 
  return (
    <Page
      className={classes.root}
      title="Customers"
    >
      <Container maxWidth={false}>
        <Toolbar />
        <Box mt={3}>
          <Results customers={customers} />
        </Box>
      </Container>
    </Page>
  );
};

export default CustomerListView;
