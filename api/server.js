const express = require("express");
const bodyParser = require ("body-parser");
const cors = require("cors");

const app = express();

 app.use((req, res, next) => {
		//res.header('Access-Control-Allow-Origin', '*');
	

		res.setHeader("Access-Control-Allow-Origin", "*");
		res.setHeader("Access-Control-Allow-Credentials", "true");
		res.setHeader("Access-Control-Max-Age", "1800");
		res.setHeader("Access-Control-Allow-Headers", "content-type");
		res.setHeader("Access-Control-Allow-Methods","PUT, POST, GET, DELETE, PATCH, OPTIONS");
		// res.setHeader("Content-Type", "application/json;charset=utf-8"); // Opening this comment will cause problems
		next();
  });

app.use((req, res, next) => {
req.header('Access-Control-Allow-Origin', '*');
	next();
  });

const db = require("./app/models");
db.sequelize.sync();

//app.use(cors(corsOptions));

app.use(bodyParser.json());

app.use(bodyParser.urlencoded({extended: true}));

app.get("/", (req, res) =>{
	res.json({message: "Servidor Soportado por Node.js"});
});

require("./app/routes/userlab.routes")(app);
require("./app/routes/product.routes")(app);
require("./app/routes/sale.routes")(app);
require("./app/routes/detalle.routes")(app);


const PORT = process.env.PORT || 8080;
app.listen(PORT, () => {
	console.log(`Server is running on port ${PORT}.`);
})