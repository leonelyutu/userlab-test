module.exports = (sequelize, Sequelize) => {
    const Sale = sequelize.define("sale", {
      id_producto: {
        type: Sequelize.INTEGER
      },
      id_cliente: {
        type: Sequelize.INTEGER,
        references: {         // User belongsTo Company 1:1
          model: 'userlab',
          key: 'id'
        }
        
      },
      id_detalle: {
        type: Sequelize.INTEGER
      },
      
    });
  
    return Sale;
  };
  