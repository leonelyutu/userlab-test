module.exports = app => {
  const userlabs = require("../controllers/userlab.controller.js");

  var router = require("express").Router();

  // Create a new Tutorial
  router.post("/", userlabs.create);

  // Retrieve all Tutorials
  router.get("/", userlabs.findAll);

  // Retrieve all published Tutorials
  router.get("/published", userlabs.findAllPublished);

  // Retrieve a single Tutorial with id
  router.get("/:id", userlabs.findOne);

  // Update a Tutorial with id
  router.put("/:id", userlabs.update);

  // Delete a Tutorial with id
  router.delete("/:id", userlabs.delete);

  // Create a new Tutorial
  router.delete("/", userlabs.deleteAll);

  app.use('/api/userlabs', router);
};