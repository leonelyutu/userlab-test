module.exports = app => {
    const detalles = require("../controllers/detalle.controller.js");
  
    var router = require("express").Router();
  
    // Create a new 
    router.post("/", detalles.create);
  
    // Retrieve all 
    router.get("/", detalles.findAll);
  
    // Retrieve all published 
    router.get("/published", detalles.findAllPublished);
  
    // Retrieve a single  with id
    router.get("/:id", detalles.findOne);
  
    // Update a  with id
    router.put("/:id", detalles.update);
  
    // Delete a  with id
    router.delete("/:id", detalles.delete);
  
    // Create a new 
    router.delete("/", detalles.deleteAll);
  
    app.use('/api/detalles', router);
  };