module.exports = app => {
    const sales = require("../controllers/sale.controller.js");
  
    var router = require("express").Router();
  
    // Create a new}
    router.post("/", sales.create);
  
    // Retrieve all }
    router.get("/", sales.findAll);
  
    // Retrieve all published }
    router.get("/published", sales.findAllPublished);
  
    // Retrieve a single } with id
    router.get("/:id", sales.findOne);
  
    // Update a with id
    router.put("/:id", sales.update);
  
    // Delete a  with id
    router.delete("/:id", sales.delete);
  
    // Create a new 
    router.delete("/", sales.deleteAll);
  
    app.use('/api/sales', router);
  };