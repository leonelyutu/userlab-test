const db = require("../models");
const Product = db.products;
const Op = db.Sequelize.Op;

// Create and Save a new user lab
exports.create = (req, res) => {
  // Validate request
  if (!req.body.descripcion) {
    res.status(400).send({
      message: "Content can not be empty!"
    });
    return;
  }

  // Create a Tutorial
  const product = {
    descripcion: req.body.descripcion,
    precio: req.body.precio,
    id_categoria: req.body.id_categoria,
    id_proveedor: req.body.id_proveedor,
    stock: req.body.stock,
   
  };

  // Save  in the database
  Product.create(product)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the userlab."
      });
    });
};

// Retrieve all  from the database.
exports.findAll = (req, res) => {
   const descripcion = req.query.descripcion;
  var condition = descripcion ? { descripcion: { [Op.iLike]: `%${descripcion}%` } } : null;

  Product.findAll({ where: condition })
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving tutorials."
      });
    });
};

// Find a single  with an id
exports.findOne = (req, res) => {
    const id = req.params.id;

  Product.findByPk(id)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message: "Error retrieving Tutorial with id=" + id
      });
    });
};

// Update a  by the id in the request
exports.update = (req, res) => {
    const id = req.params.id;

  Product.update(req.body, {
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        res.send({
          message: "Tutorial was updated successfully."
        });
      } else {
        res.send({
          message: `Cannot update Tutorial with id=${id}. Maybe Tutorial was not found or req.body is empty!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Error updating Tutorial with id=" + id
      });
    });
};

// Delete a  with the specified id in the request
exports.delete = (req, res) => {
	 const id = req.params.id;

  Product.destroy({
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        res.send({
          message: "Tutorial was deleted successfully!"
        });
      } else {
        res.send({
          message: `Cannot delete Tutorial with id=${id}. Maybe Tutorial was not found!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Could not delete Tutorial with id=" + id
      });
    });
  
};

// Delete all  from the database.
exports.deleteAll = (req, res) => {
	 Product.destroy({
    where: {},
    truncate: false
  })
    .then(nums => {
      res.send({ message: `${nums} Tutorials were deleted successfully!` });
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while removing all tutorials."
      });
    });
  
};

// Find all published 
exports.findAllPublished = (req, res) => {
	 Product.findAll({ where: { published: true } })
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving tutorials."
      });
    });
  
};