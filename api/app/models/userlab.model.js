module.exports = (sequelize, Sequelize) => {
  const Userlab = sequelize.define("userlab", {
    nombre: {
      type: Sequelize.STRING
    },
    apellido: {
      type: Sequelize.STRING
    },
    correo: {
      type: Sequelize.STRING
    },
    telefono_movil: {
      type: Sequelize.STRING
    },
    telefono_fijo: {
      type: Sequelize.STRING
    },
    dpi_Identificacion: {
      type: Sequelize.STRING
    }
  });

  return Userlab;
};