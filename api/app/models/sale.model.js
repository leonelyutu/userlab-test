module.exports = (sequelize, Sequelize) => {
    const Sale = sequelize.define("sale", {
      id_producto: {
        type: Sequelize.INTEGER,
        references: {         // User belongsTo Company 1:1
          model: 'products',
          key: 'id'
        }
      },
      id_cliente: {
        type: Sequelize.INTEGER,
        references: {         // User belongsTo Company 1:1
          model: 'userlabs',
          key: 'id'
        }
      },
     
      
    });
  
    return Sale;
  };