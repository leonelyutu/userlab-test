module.exports = (sequelize, Sequelize) => {
    const Detalle = sequelize.define("detalle", {
      id_venta: {
        type: Sequelize.INTEGER,
        references: {         // User belongsTo Company 1:1
          model: 'sales',
          key: 'id'
        }
        
      },
      id_producto: {
        type: Sequelize.INTEGER,
        references: {         // User belongsTo Company 1:1
          model: 'products',
          key: 'id'
        }
      },
      cantidad: {
        type: Sequelize.INTEGER
      },
      total: {
        type: Sequelize.INTEGER
      },
      
    });
  
    return Detalle;
  };