module.exports = (sequelize, Sequelize) => {
  const Product = sequelize.define("product", {
    descripcion: {
      type: Sequelize.STRING
    },
    precio: {
      type: Sequelize.STRING
    },
    id_categoria: {
      type: Sequelize.STRING
    },
    id_proveedor: {
      type: Sequelize.STRING
    },
    stock: {
      type: Sequelize.STRING
    }
  });

  return Product;
};